
.PHONY: all
all: html pdf

.PHONY: pdf
pdf:
	a2x --verbose -f pdf \
		-a lang=es \
		--dblatex-opts="-P doc.publisher.show=0" \
		--dblatex-opts="-P latex.output.revhistory=0" \
		comunicadolorea.asciidoc

.PHONY: html
html:
	asciidoc --verbose \
		-a toc2 \
		comunicadolorea.asciidoc

.PHONY: clean
clean:
	rm -rf images *.pdf *.html *.fo *.sty *.css
